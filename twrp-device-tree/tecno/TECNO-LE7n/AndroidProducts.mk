#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TECNO-LE7n.mk

COMMON_LUNCH_CHOICES := \
    omni_TECNO-LE7n-user \
    omni_TECNO-LE7n-userdebug \
    omni_TECNO-LE7n-eng
